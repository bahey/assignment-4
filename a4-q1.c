#include <stdio.h>
int main()
{
	//Variable Diclaration
	int num1, num2, num3, sum, average;
	
	//Input the numbers
	printf("Enter three numbers :");
	scanf("%d %d %d", &num1, &num2, &num3);
	
	//calculating the needs
	sum = num1 + num2 + num3;
	average = sum/3;
	
	//Printing the outputs
	printf("\nThe Sum of your numbers is: %d", sum);
	printf("\nThe Average of your numbers is: %d", average);
	
}
