#include <stdio.h>
int main()
{
	//Variable Diclaration
	int num1, num2;
	
	//Input the numbers
	printf("Enter number one: ");
	scanf("%d", &num1);

	printf("Enter number two: ");
	scanf("%d", &num2);

	//Swapping numbers
	num1 = num1 + num2;
	num2 = num1 - num2;
	num1 = num1 - num2;
	
	//Printing the outputs
	printf("\nThe swapped numbers: %d , %d", num1, num2);

	
}
