#include <stdio.h>
int main()
{
	//Variable Diclaration
	int height, radius;
	float volume, pi;
	
	//Input the height
	printf("Enter the height of the cone: ");
	scanf("%d", &height);

	//Input the radius
	printf("Enter the radius of the cone: ");
	scanf("%d", &radius);
		
	//calculating the needs
	pi = 3.1472;
	volume = (1.0/3) * pi * radius * radius * height;

	
	//Printing the outputs
	printf("\nThe Volume of the cone is: %.3f", volume);

	
}
